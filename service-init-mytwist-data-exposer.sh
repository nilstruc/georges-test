#!/bin/bash

# init service wave-status-api
# 2017-01-25 nils TRUCHAUD

INSTALLER_DIR=`pwd`
DEP_DATE=`date +%Y%m%d%H%M%S`


cd $INSTALLER_DIR
if [ -f "services/mytwist-data-exposer.service" ]
then

   echo "mise en place du service data-exposer"

    cp services/mytwist-data-exposer.service /etc/systemd/system/wave-status-api.service
    systemctl enable mytwist-data-exposer.service

     echo "demarage de mytwist-data-exposer.service"
     systemctl start mytwist-data-exposer.service
     echo "status de  mytwist-data-exposer.service "
     systemctl status mytwist-data-exposer.service
fi
