var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */
var PromoCode = new keystone.List('PromoCode', {
	map: {name: 'name'}
});

PromoCode.add({
	name: { type: Types.Text, default: "", required: true },
	avantage: { type: Types.Number, default: 0, required: true },
	restrictions: {
		age: {
			rest: { type: Types.Select, options: 'Greater than, Less than, Equal' },
			number: { type: Types.Number, default: 0 },
		},
		startDate: { type: Types.Date, default: Date.now },
		endDate: { type: Types.Date, default: Date.now },
		meteo: {
			is: { type: Types.Select, options: 'clear sky, few clouds, scattered clouds, broken clouds, shower rain, rain, thunderstorm, snow, mist' },
			temp: {
				rest: { type: Types.Select, options: 'Greater than, Less than, Equal' },
				celsius: { type: Types.Number, default: 0 },
			},
		},
	}
});


/**
 * Registration
 */
PromoCode.defaultColumns = 'name';
PromoCode.register();
