keystone = require('keystone');
request = require('request');

var config = require('../../config.js');

var PromoCode = keystone.list('PromoCode');

exports.promoCheck = function(req, res) {
  console.log("Hi !");
  var promoCodeName = "";


	PromoCode.model.findOne().exec(function(err, promoCode) {
		if (err) return res.apiResponse({statusCode: 500, message: 'An error occured, please try again later.'});
 		if (!promoCode) return res.apiResponse({ statusCode: 400, message: "We weren't able to find any promo code at the moment. Come back later!" });

    promoCodeName = promoCode.name;

    var town = req.params.town;
    var age = req.params.age;

    var rslt = false;
    var reason = "";
    var ageTest = "";
    var weatherTest = "";
    var weatherValueTest = "";

    // Age condition
    ageTest = ageCompare(age, promoCode);
    if (!ageTest) {
      reason = "You don't have the right age, sorry!";
      rslt = false;
    }



    // Meteo condition
    var url = 'http://api.openweathermap.org/data/2.5/weather?q=' + town + '&APPID=' + config.settings.openWeatherMapApiKey + '&units=metric';

    request(url, function(error, response, body){
      console.log(body);
      var jsonBody = JSON.parse(body);

      var is = jsonBody.weather[0].description;
      var temp = jsonBody.main.temp;

      console.log(is);
      console.log(temp);

      weatherTest = weatherCompare(temp, promoCode);
      if (!weatherTest){
        rslt = false;
        reason = "This is not the right weather, sorry!";
      }

      if(is == promoCode.restrictions.meteo.is){
        weatherValueTest = true
      }
      else {
        weatherValueTest = false;
        reason = "This is not the right weather, sorry!";
      }

      console.log(ageTest);
      console.log(weatherTest);
      console.log(weatherValueTest);

      if (ageTest && weatherTest && weatherValueTest){
        rslt = true
      }

      if (rslt){
        res.apiResponse({
          promocode_name: promoCodeName,
          status: 'accepted',
          avantage: promoCode.avantage
        })
      }
      else {
        res.apiResponse({
          promocode_name: promoCodeName,
          status: 'denied',
          reason: reason
        })
      }
    })
	})
}

var ageCompare = function(age, promoCode) {
  var rslt = false;
  switch (promoCode.restrictions.age.rest) {
    case "Greater than":
      if(age >= promoCode.restrictions.age.number) {
        rslt = true;
      }
      break;

    case "Less than":
    if(age <= promoCode.restrictions.age.number) {
      rslt = true;
    }
    break;

  default:
    if(promoCode.restrictions.age.number == age) {
      rslt = true;
    }
    break;
  }

  return rslt;
}

var weatherCompare = function(temp, promoCode) {
  var rslt = false;

  switch (promoCode.restrictions.meteo.temp.rest) {
    case "Greater than":
      if(Math.round(temp) >= promoCode.restrictions.meteo.temp.celsius) {
        rslt = true;
      }
      else {
        rslt = false
      }
      break;

    case "Less than":
    if(Math.round(temp) <= promoCode.restrictions.meteo.temp.celsius) {
      rslt = true;
    }
    else {
      rslt = false
    }
    break;

  default:
    if(promoCode.restrictions.meteo.temp.celsius == Math.round(temp)) {
      rslt = true;
    }
    else {
      rslt = false
    }
    break;
  }
  return rslt;
}
