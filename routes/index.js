/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

var config = require('../config.js');

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	api: importRoutes('./api'),
};

// Setup Route Bindings
exports = module.exports = function (app) {

	// app.all('/api*', function(req, res, next) {
	// 	if (req.headers.apikey == config.settings.api_secret)
	// 		return next();
	// 	else
	// 		return res.status(401).json({'statusCode': '401', 'message': 'Unauthorized – Authentication is required to access the resource'})
	// });

	// Views

	app.all('/', routes.views.index);

	app.get('/api/promoCheck/:age/:town', keystone.middleware.api, routes.api.promoCheck.promoCheck);

	// app.get('/api/v1/inventory', keystone.middleware.api, routes.api.db_exposer.getDevices);


	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);

};
