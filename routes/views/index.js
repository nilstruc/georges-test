var keystone = require('keystone');
var PromoCode = keystone.list('PromoCode');
var request = require('request');


exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';

	view.on('post', {action: 'send'}, function(next){

		console.log("Req Body : " + req.body.age);
		console.log("Req Body : " + req.body.city);

		request('http://localhost:8000/api/promoCheck/' + req.body.age + '/' + req.body.city, function(error, response, body){
			console.log("Body : " + body);
			res.redirect('http://localhost:8000/api/promoCheck/' + req.body.age + '/' + req.body.city);
		})

	})

	// Render the view
	view.render('index');
};
