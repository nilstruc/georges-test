#!/bin/bash

# build my twist data-exposer
# 2017-011-30 nils TRUCHAUD

INSTALLER_DIR=`pwd`
DEP_DATE=`date +%Y%m%d%H%M%S`

DEP_BRANCH=master

if [ -f "$INSTALLER_DIR/custom/mytwist-data-exposer/repo.conf" ]
then

  . $INSTALLER_DIR/custom/mytwist-data-exposer/repo.conf

fi

cd /opt/home/dd-dep/build/


if [ -d "mytwist-data-exposer/" ]
then
    echo "instalation existante mytwist-data-exposer/"
    rm -Rf mytwist-data-exposer/
fi



git clone -b $DEP_BRANCH git@gitlab.mobilitybox.net:nils/chatMeUp_data_exposer.git


if [ -n "${DEP_TAG}" ]
then
        echo "(custom git from BRANCH : $DEP_BRANCH - DEP_TAG : $DEP_TAG )"

        cd /opt/home/dd-dep/build/mytwist-data-exposer

        git checkout tags/$DEP_TAG

fi

echo "( git from   BRANCH : $DEP_BRANCH - TAG : $DEP_TAG )"



cd /opt/home/dd-dep/build/mytwist-data-exposer

npm install

cd /opt/home/dd-dep/build/wave-status-api/node_modules/keystone

npm install
