#!/bin/bash

# dep builded sources myTwist data exposer
# 2017-01-25 nils TRUCHAUD


INSTALLER_DIR=`pwd`
DEP_DATE=`date +%Y%m%d%H%M%S`



cd /opt/node/myTwist/

if [ -d "mytwist-data-exposer/" ]
then
    echo "instalation existante  de mytwist-data-exposer/ dans /opt/node/wave/wave-status-api/"
    echo "archivage de l'install vers /opt/history/"

    mkdir -p /opt/history/application/

    cd /opt/node/myTwist/

#    zip -r /opt/history/application/wave-status-api-$DEP_DATE.zip wave-status-api/
    tar czf /opt/history/application/mytwist-data-exposer-$DEP_DATE.tar.gz mytwist-data-exposer/
    echo "fin archivage de l'existant vers /opt/history/application/mytwist-data-exposer-$DEP_DATE.tar.gz"

     chown dd-dep:dd-dep -R /opt/history/application/
fi



mkdir -p /opt/node/myTwist/mytwist-data-exposer/

#cp -R -d /opt/home/dd-dep/build/wave-status-api/* /opt/node/wave/wave-status-api/
#rsync -rpog --delete /opt/home/dd-dep/build/wave-status-api /opt/node/wave/
rsync -rpogl --delete --force  --exclude=".git/" /opt/home/dd-dep/build/mytwist-data-exposer/ /opt/node/myTwist/mytwist-data-exposer

cd $INSTALLER_DIR
if [ -f "custom/mytwist-data-exposer/config.js" ]
then
      echo "custom/mytwist-data-exposer/config.js existe"
      cp custom/mytwist-data-exposer/config.js /opt/node/myTwist/mytwist-data-exposer/
fi

chown dd-dep:dd-dep -R /opt/node/myTwist/mytwist-data-exposer/
